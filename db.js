var knex = require('knex')({
    client: 'pg',
    connection: {
      host    : process.env.DB_URL          || 'localhost',
      user    : process.env.DB_USER         || 'book_user',
      password: process.env.DB_PASSWORD     || '',
      database: process.env.DB_NAME         || 'book_app',
      charset : 'utf8',
    },
  });

var bookshelf = require('bookshelf')(knex);
var rawQuery  = bookshelf.knex.raw;

const Author  = bookshelf.Model.extend({ tableName: 'authors' });
const Book    = bookshelf.Model.extend({ tableName: 'books' });

module.exports = {
    rawQuery,
    Book,
    Author
}