const express    = require("express");
const bodyParser = require('body-parser')

const app        = express();
const ctrl       = require("./controller");
const {ApiError}   = require("./apiError");
const port       = process.env.PORT || "8080";

const {authenticate, requestUtil} = require('./middleware');

app.use(bodyParser.json())
app.use(requestUtil);
app.use(authenticate);

app.get("/"                 , wrap(ctrl.appHome));
app.get("/api"              , wrap(ctrl.apiHome));

app.post("/api/books"       , wrap(ctrl.addBook));
app.get("/api/books"        , wrap(ctrl.getBookList));
app.get("/api/books/:id"    , wrap(ctrl.getSingleBookDetails));
app.put("/api/books/:id"    , wrap(ctrl.updateBookDetails));

app.post("/api/authors"     , wrap(ctrl.addAuthor));
app.get("/api/authors"      , wrap(ctrl.getAuthorList));
app.get("/api/authors/:id"  , wrap(ctrl.getSingleAuthorDetails));
app.put("/api/authors/:id"  , wrap(ctrl.updateAuthorDetails));

app.use(function (req, res) {
    res.status(404).json({ error: true, message: 'Page not found' });
});

app.use((err, req, res, next) => {
    if(typeof err === 'string') return res.status(400).json({error:true, message: err});
    if(err instanceof ApiError) return res.status(err.status).json(err.response);
    if(err.message == "EmptyResponse")
    return res.status(404).json({error: true, message: "Record not found"});

    res.status(500).json({error:true, message: err.name, debug: err.toString()});
});


app.listen(port, () => console.log(`App listening on port ${port}!`)).on('error', err => {
    if(err.code=='EADDRINUSE'){
        console.log(`Port ${err.port} alread in use.`);
    }
    console.log(err);
    process.exit(1);
});

function wrap(func) {
    return async function (req, res, next) {
      try {
        await func(req, res, next);
      } catch (err) {
        next(err);
      }
    };
}