const { MissingParamsError, BadRequestError } = require('./apiError');

exports.authenticate = function(req, res, next){
    let data = 'c3RhY2thYnVzZS5jb20=';  
    let buff = new Buffer(data, 'base64');  
    let text = buff.toString('ascii');
    next();
}

exports.requestUtil = (req, res, next) => {
    /* res.data() to send json data */
    res.data = (data, message = '', meta) => {
      const response = { error: false, message, data };
      if (meta) response.meta = meta;
      res.json(response);
    };
  
    /* attach validation functions */
    req.ensureQueryHas = ensure(req.query);
    req.ensureParamHas = ensure(req.params);
    req.ensureBodyHas = ensure(req.body);
    req.ensureJSONBody = ensureJSONBody;
    req.validateField = validateField;
    next();
  
    /**
     * Ensure parameters with request
     * @function ensure
     * @param  {Object} source location to check: req.body | req.params | req.query
     * @throws {MissingParamsError} When parameter not found
     * @return {void}
     */
    function ensure(source) {
  
      return (fields) => {
        if (!Array.isArray(fields)) throw new Error('ensure() accepts array only');
        for (const field of fields) {
          if (typeof source[field] === 'undefined' || source[field] === '' || source[field] === null){
            throw new MissingParamsError(`Parameter '${field}' is required`);
          }
        }
      };
    }
  
    function validateBodyField(field, rgx){
      if(typeof req.body[field] === 'string')
        req.body[field] = req.body[field].trim();
  
      if(typeof rgx === 'function'){
        if(!rgx(req.body[field])) throw new BadRequestError(`${field} is invalid`);
      }else{
        if(typeof rgx === 'string') rgx = RegExp(rgx);
        if(!rgx.test(req.body[field])) throw new BadRequestError(`${field} is invalid`);
      }
    }
  
  
    /**
     * Ensure body params has a valid JSON
     * @function ensureJSONBody
     * @param  {Array} fields Array of fields that need to be validated
     * @throws {BadRequestError} for invalid JSON
     * @return {void}
     */
    function ensureJSONBody(fields) {
      if (!Array.isArray(fields)) throw new Error('ensureJSONBody() accepts array only');
      for (const field of fields) {
        try {
          if (typeof req.body[field] === 'string') {
            req.body[field] = JSON.parse(req.body[field]);
          }
        } catch (e) {
          console.log(e);
          throw new BadRequestError(`Param ${field} must to be a valid JSON`);
        }
      }
    }
  };
  
  
  function validateField(fieldName,input, validationSchema){
    if(!fieldName) fieldName = '';
    if(typeof validationSchema != 'object') throw new Error('Schema is not an object');
    if(!validationSchema._type){
      let keys = Object.keys(validationSchema);
      for(let key of keys){
        if(fieldName.length !== 0 && fieldName[fieldName.length-1] != '.') fieldName = fieldName+ '.';
        if(!input) input={}; // creating empty object to go for next validation
        validateField(fieldName+key, input[key], validationSchema[key]);
      }
      return;
    }
  
    if(!input && validationSchema._required === true) throw new BadRequestError(`Field '${fieldName}' is required`);
    if(input && validationSchema._type === 'array'){
      if(!Array.isArray(input))
        throw new BadRequestError(`Field '${fieldName}' should be a ${validationSchema._type}`);
    }
    if(typeof input !== 'undefined' && typeof input !== validationSchema._type && validationSchema._type !== 'array'){
      throw new BadRequestError(`Field '${fieldName}' should be a ${validationSchema._type}`);
    }
  
    if(validationSchema._type === 'string' && validationSchema._validation instanceof RegExp){
      // console.log(input, validationSchema._validation, validationSchema._validation.test(input));
      if(input && validationSchema._validation.test(input) !== true)
        throw new BadRequestError(`Field '${fieldName}' is invalid`);
    }
  
    if(typeof validationSchema._validation === 'function'){
      if(input && validationSchema._validation(input) !== true)
        throw new BadRequestError(`Field '${fieldName}' is invalid`);
    }
  }
  