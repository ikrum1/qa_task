function isValidURL(str){
    if(typeof str != 'string') return false;
  
    let regexp =  /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
    if (regexp.test(str)) return true;
    return false;
}

function isValidEmail(str){
    let regexp =  /^[a-z][a-z0-9\.]*[a-z0-9]@[a-z][a-z0-9\-\.]*\.[a-z]{2,10}$/;
    if (regexp.test((str || "").toLowerCase())) return true;
    return false;
}

function isValidMobile(str){
    let regexp =  /^[\+]?(88)?01[6,7,8,9][0-9]{8}$/;
    if (regexp.test(str)) return true;
    return false;
}


exports.authorSchema = {
    name   : { _required: true, _type: 'string', _validation: /^[A-z\.\- ]{4,100}$/},
    email  : { _required: true, _type: 'string', _validation: isValidEmail},
    mobile : { _required: true, _type: 'string', _validation: isValidMobile},
    address: { _required: true, _type: 'string', _validation: /^[A-z0-9\.\- \,]{4,100}$/},
}
exports.bookSchema = {
    author_id      : { _required: true, _type: 'number' },
    title      : {
        en       : { _required: true, _type: 'string', _validation: /^[A-z0-9\.\- ]{4,100}$/},
        bn       : { _required: true, _type: 'string', _validation: /^.{4,200}$/}
    },
    description : {
        en       : { _required: true, _type: 'string', _validation: /^[A-z0-9\.\- ]{4,200}$/},
        bn       : { _required: true, _type: 'string', _validation: /^.{4,300}$/}
    },
    status        : { _required: false, _type: 'string', _validation: /^((Active)|(InActive))$/},
    image_url     : { _required: false, _type: 'string', _validation: isValidURL},
};