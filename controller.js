const {rawQuery,Book,Author} = require('./db');
const {authorSchema, bookSchema} = require('./validator');

exports.appHome = function(req,res,next){
    res.json({error: false, message: "App home", data: [
        {api_endpoint: "/api", example: "localhost:8080/api"},
    ]});
}
exports.apiHome = function(req,res,next){
    res.json({error: false, message: "API Homepage", data: {
        book_api: [
            {
                apiName        : "addBook",
                method         : "POST",
                endpoint       : "/api/books",
                parameters     : ['title','description','author_id', 'image_url'],
                example        : {
                    "title"      : {
                        "en"     : "My Book name in english",
                        "bn"     : "My Book name in bangla"
                    },
                    "description": {
                        "en"     : "Book description in english",
                        "bn"     : "Book description in bangla"
                    },
                    "author_id"  : 123,
                    "image_url"  : "http: //example.com/image.jpg"
                }
            },
            {
                apiName        : "getBookList",
                method         : "GET",
                endpoint       : "/api/books",
                parameters     : []
            },
            {
                apiName        : "getSingleBookDetails",
                method         : "GET",
                endpoint       : "/api/books/:bookId",
                parameters     : []
            },
            {
                apiName        : "updateBookDetails",
                method         : "PUT",
                endpoint       : "/api/books/:bookId",
                parameters     : []
            },
        ],
        author_api: [
            {
                apiName        : "addAuthor",
                method         : "POST",
                endpoint       : "/api/authors",
                parameters     : ['name','email','mobile', 'address']
            },
            {
                apiName        : "getAuthorList",
                method         : "GET",
                endpoint       : "/api/authors",
                parameters: []
            },
            {
                apiName        : "getSingleAuthorDetails",
                method         : "GET",
                endpoint       : "/api/authors/:authorID",
                parameters: []
            },
            {
                apiName        : "updateAuthorDetails",
                method         : "PUT",
                endpoint       : "/api/authors/:authorID",
                parameters: []
            },
        ]}
    });
}

/*
 * Book Controllers
 */
exports.addBook = async function(req,res,next){
    req.ensureBodyHas(['title', 'description', 'author_id', 'image_url']);
    req.validateField(null, req.body, bookSchema);

    new Book({
        title      : req.body.title,
        description: req.body.description,
        author_id  : req.body.author_id,
        image_url  : req.body.image_url,
    }).save().then(res.data, "Book added").catch(next);
}

exports.getBookList = async function(req,res,next){
    Book.fetchAll().then(res.data, "List of books").catch(next);
}

exports.getSingleBookDetails = async function(req,res,next){
    Book.forge({id: req.params.id}).fetch({require: true}).then(res.data, "Details of a book").catch(next);
}

exports.updateBookDetails = async function(req,res,next){
    const book = await Book.forge({id: req.params.id}).fetch({require: true});
    book.save({
        title      : req.body.title         || book.get("title"),
        description: req.body.description   || book.get("description"),
        author_id  : req.body.author_id     || book.get("author_id"),
        image_url  : req.body.image_url     || book.get("image_url"),
    }).then(res.data, "Book Updated").catch(next);
}

/*
 * Author Controllers
 */
exports.addAuthor = async function(req,res,next){
    req.ensureBodyHas(['name', 'email', 'mobile', 'address']);
    req.validateField(null, req.body, authorSchema);

    new Author({
        name   : req.body.name,
        mobile  : req.body.mobile,
        email  : req.body.email,
        address: req.body.address,
    }).save().then(res.data, "Author added").catch(next);
}

exports.getAuthorList = async function(req,res,next){
    Author.fetchAll().then(res.data, "List of authors").catch(next);
}

exports.getSingleAuthorDetails = async function(req,res,next){
    Author.forge({id: req.params.id}).fetch({require: true}).then(res.data, "Details of a author").catch(next);
}

exports.updateAuthorDetails = async function(req,res,next){
    const author = await Author.forge({id: req.params.id}).fetch({require: true});
    author.save({
        name   : req.body.name      || author.get("name"),
        email  : req.body.email     || author.get("email"),
        mobile  : req.body.mobile     || author.get("email"),
        address: req.body.address   || author.get("address"),
    }).then(res.data, "Author Updated").catch(next);
}

