## Task For QA Automation
This is a simple REST API which has only two entities `Author and Book` where there are couple of APIs to make the CRUD on each of them

## Running the application
Considering you have docker installed on your pc and there is no other application running on port `8080`, run the below command
```
# Build And run the api server
docker-compose up --build

# stop the server
CTRL+C  on the active terminal

OR

docker-compose down 
```

it will run the application as bellow:
```
API server -> localhost:8080
Database -> localhost:5433
```

## API Documentation
Visit the homepage of the api (`localhost:8080`) where you will get the necessary information which will guide you to get the endpoints and their parameters.

# Tasks

### Instructions
* You can't change any code in the API server. Infact, You don't even need to see the code of the server.
* Consider it a Black Box testing
* Max 3 days to finish the task, the sooner the better.
* Create seperate folder for each task and put all of the work in one folder. Zip everything as `solution.zip` and send it as an attachemnt.
* It's not necessary to add/write the task description to any answer/code.

## Task 1 ( Manual Testing )
* Create an Excell file and list out all the test cases you want cover
* Do manual testing to all the APIs and mark them as (PASSED, FAILED) in the Excell file
* Categorize the failed cases as ( Critical,  Major, Normal, Minor) Bug/Issue
* Put some necessary comment when test case failed so that some one ce reproduce the issue or BUG


## Task 2 ( Test Automation )
* Write code in any programming language to automate the API tests.
* Code should be configured for two different module so that you can run any individual module or all.
pattern:
```
YOUR_TEST_COMMAND MODULE_NAME

example: 
    go test author # will test the author module only (If you write in GoLang)

    npm test author  # will test the author module only (For Node)

    behave author.features  # will test the author module only (For python/behave)

    npm test # when MODULE_NAME is not specified it will run the full test suite.
```

* Share the instructions or commands how to run the test suit and what environment does it requires to run, in a `.docx` or `README.md` file
* Attach screenshot of the output of your local machine after the running the test suit.

## Task 3 | Database screenshot
* After running all the test cases on your machine, connect the local database using any GUI tool (phpmyadmin, adminer, Navicat etc) and take screenshot of both authors and books table with the data

## Good to have
Feel free to add any comment about the tasks or share your learning from the task. Also we welcome code review.






