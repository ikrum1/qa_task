FROM node:8.12.0-alpine

WORKDIR /app

ADD package.json /app/package.json
RUN npm install

ADD . /app

