CREATE TABLE "books" (
  "id" serial NOT NULL,
  "author_id" integer NOT NULL,
  "title" jsonb NOT NULL,
  "description" jsonb NOT NULL,
  "status" character varying(25) NOT NULL DEFAULT 'Active',
  "image_url" character varying(200) NOT NULL,
  "created_at" timestamp NOT NULL DEFAULT now(),
  "updated_at" timestamp NOT NULL DEFAULT now()
);


CREATE TABLE "authors" (
  "id" serial NOT NULL,
  "name" character varying(90) NOT NULL,
  "email" character varying(50) NOT NULL,
  "mobile" character varying(13) NOT NULL,
  "address" character varying(150) NOT NULL,
  "created_at" timestamp NOT NULL DEFAULT now(),
  "updated_at" timestamp NOT NULL DEFAULT now()
);