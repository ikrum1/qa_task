/**
 * @module Errors
 * @summary all errors here
 */

/**
 * @memberof module:Errors
 * @summary class ApiError will be used to parse custom api errors accross the system
 */
class ApiError {
    /**
     * @constructor
     *
     * @param  {string} name    Name of the error
     * @param  {string} message Message of the error
     * @param  {Number} status  Http staus code
     * @return {void}
     */
    constructor(name, message, status, errorCode,debugMsg) {
      const msg = message.toString().replace(/"/g, "'").replace('Sequelize', '');
      this.name = name || 'ApiError';
      this.message = msg;
      this.status = status || 500;
      this.debugMsg = debugMsg;
      if(errorCode)
        this.error_code = errorCode;
    }
  
    /**
     * @static
     * @return {object} Response body of an error
     */
    get response() {
      return { error: true, error_code: this.error_code, message: this.message };
    }
  }
  
  /**
   * @memberof module:Errors
   * @extends ApiError
   */
  class ServerError extends ApiError {
    /**
     * @constructor
     * @param  {String} [message='Internal Server Error']     message for the bad request
     * @return {void}
     */
    constructor(message, debugMsg) {
      message = message || 'Internal Server Error';
      super('ServerError', message, 500,null,debugMsg);
    }
  }
  
  /**
   * @memberof module:Errors
   * @extends ApiError
   */
  class BadRequestError extends ApiError {
    /**
     * @constructor
     * @param  {String} [message='Invalid request']     message for the bad request
     * @return {void}
     */
    constructor(message = 'Invalid request', errorCode) {
      super('BadRequestError', message, 400, errorCode);
    }
  }
  /**
   * @memberof module:Errors
   * @extends ApiError
   */
  class MissingParamsError extends ApiError {
    /**
     * @constructor
     * @param  {String} [message='Missing Required parameter(s)'] message for the missing params
     * @return {void}
     */
    constructor(message = 'Missing Required parameter(s)') {
      super('MissingParamsError', message, 400);
    }
  }
  
  /**
   * @memberof module:Errors
   * @extends ApiError
   */
  class InvalidTokenError extends ApiError {
    /**
     * @constructor
     * @param  {String} [message='Authorization token is invalid'] message for the InvalidTokenError
     * @return {void}
     */
    constructor(message = 'Authorization token is invalid') {
      super('InvalidTokenError', message, 401);
    }
  }
  /**
   * @memberof module:Errors
   * @extends ApiError
   */
  class InvalidAccessError extends ApiError {
    /**
     * @constructor
     * @param  {String} [message='Access denied']      message for InvalidAccessError
     * @return {void}
     */
    constructor(message = 'Access denied') {
      super('InvalidAccessError', message, 403);
    }
  }
  /**
   * @memberof module:Errors
   * @extends ApiError
   */
  class RecordNotFoundError extends ApiError {
    /**
     * @constructor
     * @param  {String} [message='Record not found'] message for RecordNotFoundError
     * @return {void}
     */
    constructor(message = 'Record not found') {
      super('RecordNotFoundError', message, 404);
    }
  }
  
  /**
   * @memberof module:Errors
   * @extends ApiError
   */
  class NotAcceptableError extends ApiError {
    /**
     * @constructor
     * @param  {String} [message='Record not found'] message for RecordNotFoundError
     * @return {void}
     */
    constructor(message = 'Not Acceptable') {
      super('NotAcceptableError', message, 406);
    }
  }
  
  module.exports = {
    ApiError,
    ServerError,
    BadRequestError,
    MissingParamsError,
    RecordNotFoundError,
    InvalidTokenError,
    InvalidAccessError,
    NotAcceptableError,
  };
  